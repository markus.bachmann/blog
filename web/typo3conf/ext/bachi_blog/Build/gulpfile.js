var gulp = require('gulp'),
    sass = require('gulp-sass'),
    webpack = require('webpack')
    wp = require('webpack-stream'),
    autoprefixer = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    browserSync = require('browser-sync').create(),
    argv = require('yargs').argv,
    path = require('path')
;

gulp.task('sass', function() {
    gulp.src('../Resources/Public/Sass/bootstrap.sass')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gulp.dest('../Resources/Public/Css'))
        .pipe(browserSync.stream());
});

gulp.task('js', function() {
    var config = require('./webpack.config.js');

    if (argv.production) {
        config.plugins.push(new webpack.optimize.UglifyJsPlugin());
    }

    gulp.src('../Resources/Public/JavaScript/Src/app.js')
        .pipe(wp(config, webpack))
        .pipe(gulp.dest('../Resources/Public/JavaScript/Dist'))
        .pipe(browserSync.stream());
});

gulp.task('image', function() {
    gulp.src('../Resources/Public/Images/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('../Resources/Public/Images'))
        .pipe(browserSync.stream());
});

gulp.task('watch', function() {
    browserSync.init({
        proxy: '127.0.0.1:9000'
    });

    gulp.watch('../Resources/Private/**/*').on('change', browserSync.reload);

    gulp.watch('../Resources/Public/Sass/**/*', ['sass']);
    gulp.watch('../Resources/Public/JavaScript/Src/**/*', ['js']);
    gulp.watch('../Resources/Public/Images/**/*', ['image']);
});

gulp.task('default', ['sass', 'js', 'image']);
