var webpack = require('webpack'),
    path = require('path');

var srcPath  = path.join(__dirname, '/../Resources/Public/JavaScript/Src'),
    distPath = path.join(__dirname, '/../Resources/Public/JavaScript/Dist');

console.log(__dirname);

module.exports = {
    context: __dirname,
    entry: {
        app: '../Resources/Public/JavaScript/Src/app.js'
    },
    devtool: '#cheap-module-eval-source-map',
    output: {
        path: distPath,
        filename: 'app.js'
    },
    resolve: {
        modules: [path.resolve(__dirname, 'node_modules')],
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/
            }
        ]
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.ProvidePlugin({
          $: 'jquery',
          jQuery: 'jquery'
        })
    ]
};
