<?php
defined('TYPO3_MODE') || die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['bachi_blog'] = 'EXT:bachi_blog/Configuration/RTE/Default.yaml';
