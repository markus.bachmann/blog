var loader = require('webfontloader');
var $ = require('jquery');

require('sidr/src/jquery.sidr.js');

loader.load({
    typekit: {
        id: 'khg3tcg'
    }
});


$(function() {
    $('.mobile_navigation').sidr({
        side: 'right',
        onOpen: function() {
            $('.mobile_navigation').addClass('open');
        },
        onClose: function() {
            $('.mobile_navigation').removeClass('open');
        }
    });
});
