<?php

/**
 * Extension Manager/Repository config file for ext "bachi_blog".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Bachi Blog',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-9.5.99',
            'fluid_styled_content' => '8.7.0-9.5.99',
            'rte_ckeditor' => '8.7.0-9.5.99',
            'news' => '6.2.0-6.3.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'MarkusBachmann\\BachiBlog\\' => 'Classes'
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Markus Bachmann',
    'author_email' => 'markus.bachmann@bachi.biz',
    'author_company' => 'Markus Bachmann',
    'version' => '1.0.0',
];
