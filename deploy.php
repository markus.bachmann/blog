<?php
namespace Deployer;

require 'recipe/typo3.php';

set('typo3_webroot', 'web');

// Project name
set('application', 'bachi-biz');

// Project repository
set('repository', 'git@gitlab.com:markus.bachmann/blog.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', ['.env']);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts

host('46.101.232.6')
    ->user('typo3')
    ->set('deploy_path', '/var/www/typo3/production');

// Tasks

task('build', function() {
    cd('{{release_path}}/web/typo3conf/ext/bachi_blog/Build');
    run('npm install && gulp sass image js');
});

task('clear:cache', function() {
    run('{{bin/php}} {{release_path}}/vendor/bin/typo3cms cache:flushgroups pages,system');
});
after('deploy:symlink', 'clear:cache');

after('deploy:update_code', 'build');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
